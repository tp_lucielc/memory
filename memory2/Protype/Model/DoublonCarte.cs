﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protype.Model
{
    public class DoublonCarte
    {
        private static int numero = 0;
        private Carte carte1 = null;
        private Carte carte2 = null;
        private Carte doublonCarte;
        private bool status = false;
        private int identifiant = 0;

        public DoublonCarte(Carte laCarte)
        {
            numero++;
            this.identifiant = identifiant + numero;
            this.carte1 = new Carte(laCarte);
            this.carte2 = new Carte(laCarte);
            this.carte1.SetDoublonCartes(this);
            this.carte2.SetDoublonCartes(this);
        }    
        public Carte GetCarte1()
        {
            return this.carte1;
        }
        public Carte GetCarte2()
        {
            return this.carte2;
        }
        public bool GetStatus()
        {
            return this.status;
        }
        public int GetIdentifiant()
        {
            return this.identifiant;
        }
        public void VerifierStatus()
        {
            if (this.carte1.GetStatus() == this.carte2.GetStatus())
            {
                this.status = true;
            }
        }
        public string Description()
        {
            return "la classe comprend :\n numero : "+numero+"\ncarte1 : "+carte1+"\ncarte2 : "+carte2+"\nstatus : "+status+"\nidentifiant : "+identifiant;
        }
    }
}
