﻿using Protype.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Protype.Model
{
    public class Carte
    {
        private static int nbreCarte = 0;
        private static int numero = 0;
        private DoublonCarte doublonCarte = null;
        private Button bouton = null;
        private bool status = false;
        private int valeur = 0;
        private string cheminImageDos = null;
        private string cheminImageFace = null;
        private Image imageDos = null;
        private Image imageFace = null;
        private string titre = null;
        private int numColFenetre = 0;
        private int numRangFenetre = 0;
        private int identifiant = 0;
        
        public Carte(string leCheminImageDos, string leCheminImageFace)
        {
            nbreCarte++;
            this.valeur= valeur + nbreCarte;
            this.cheminImageDos = leCheminImageDos;
            this.cheminImageFace = leCheminImageFace;
            this.titre = "Carte modèle n°" + nbreCarte;
            this.doublonCarte = new DoublonCarte(this);
        }
        public Carte(Carte laCarte)
        {
            numero++;
            this.identifiant = identifiant + numero;
            this.imageDos = LesServices.CreerUneImage(laCarte.GetCheminImageDos());
            this.imageFace = LesServices.CreerUneImage(laCarte.GetCheminImageFace());
            this.status = laCarte.GetStatus();
            this.bouton = new Button();
            this.AppareillerBouton();
        }

        public Button Bouton()
        {
            return this.bouton;
        }
        public bool GetStatus()
        {
            return this.status;
        }
        public void SetStatus(bool leStatus)
        {
            this.status = leStatus;
        }
        public int GetValeur()
        {
            return this.valeur;
        }
        public DoublonCarte GetDoublonCartes()
        {
            return this.doublonCarte;
        }
        public void SetDoublonCartes(DoublonCarte leDoublon)
        {
            this.doublonCarte = leDoublon;
            
        }
        public int GetIdentifiant()
        {
            return this.identifiant;
        }
        public int GetNumColFenetre()
        {
            return this.numColFenetre;
        }
        public void SetNumColFenetre(int numCol)
        {
            this.numColFenetre = numCol;
        }
        public int GetNumangFenetre()
        {
            return this.numRangFenetre;
        }
        public void SetNumRangFenetre(int numRang)
        {
            this.numRangFenetre = numRang;
        }
        public string GetCheminImageDos()
        {
            return this.cheminImageDos;
        }
        public string GetCheminImageFace()
        {
            return this.cheminImageFace;
        }
        public void AppareillerBouton()
        {
            if (this.status == false)
            {
                this.bouton.Content = this.imageDos;
            }
            else if (this.status == true)
            {
                this.bouton.Content = this.imageFace;
            }

        }
        public string Description()
        {
            return "la classe comprend : " + nbreCarte + " \nnumero :" + numero + "\ndoublonCarte "
                + doublonCarte + "\nbouton " + bouton + "\nvaleur " + valeur + "\n cheminImageDos "
                + cheminImageDos + "\n cheminImageFace " + cheminImageFace + "\ntitre " + titre
                + "\nnumColFenetre " + numColFenetre + "\nnumRangFenetre " + numRangFenetre
                + "\nidentifiant " + identifiant + "\nLe statut : " + this.status + "\nLa source image du bouton :" + ((Image)this.bouton.Content).Source;
        }

    }
}
