﻿using Protype.Controler;
using Protype.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Protype.View
{
    /// <summary>
    /// Logique d'interaction pour VueJouer.xaml
    /// </summary>
    public partial class VueJouer : Window
    {
        //tableau des chemin pour accéder aux images
        private static string[] cheminLesImages;
        //tableau des images pour le jeu
        private string[] cheminLesImagesDuJeu;
        //tableau des cartes models
        private Carte[] lesCartesModels;
        //nb de carte modele
        private Carte[] lesCartesJeu;
        //nb de carte modele
        private int nbreCarteModel = 2;
        //nb de cartes générées
        private int nbreCarte = 4;
        //nb colonnes / rangés
        private int nbreColRang = 2;
        //tableau de doublon de carte
        private DoublonCarte[] lesDoublonsCartes;
        private bool raffraichir = false;

        public VueJouer()
        {
            InitializeComponent();
            cheminLesImages = new string[19];
            for (int i = 0; i < 18; i++)
            {
                cheminLesImages[i] = "Resources/image" + i + ".jpg";
            }
            //remplissage du tableau de toutes les images de l'application
            cheminLesImagesDuJeu = new string[nbreCarteModel + 1];
            if (cheminLesImages.Length != 0)
            {
                for (int i = 0; i < nbreCarteModel + 1; i++)
                {
                    cheminLesImagesDuJeu[i] = cheminLesImages[i];
                }
            }

            //Créer des cartesModel
            lesCartesModels = new Carte[nbreCarteModel];
            for (int i = 0; i < nbreCarteModel; i++)
            {
                lesCartesModels[i] = new Carte(cheminLesImagesDuJeu[0], cheminLesImagesDuJeu[i + 1]);
            }
            //Mettre à jour les doublons
            lesDoublonsCartes = new DoublonCarte[nbreCarteModel];
            for (int i = 0; i < nbreCarteModel; i++)
            {
                lesDoublonsCartes[i] = lesCartesModels[i].GetDoublonCartes();
            }
            // et dans la classe Jouer
            Jouer.SetDoublonsDeCartes(lesDoublonsCartes);

            //Générer les cartes du jeu
            lesCartesJeu = new Carte[nbreCarte];
            for (int i = 0; i < nbreCarteModel; i++)
            {
                lesCartesJeu[i] = lesCartesModels[i].GetDoublonCartes().GetCarte1();
                lesCartesJeu[i + nbreCarteModel] = lesCartesModels[i].GetDoublonCartes().GetCarte2();
            }

            for(int i=0; i<nbreCarteModel; i++)
            {
                GdFond.RowDefinitions.Add(new RowDefinition());
                GdFond.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for(int i=0; i<nbreCarte; i++)
            {
                GdFond.Children.Add(lesCartesJeu[i].GetBouton());
            }
            int k = 0;
            for (int i = 0; i<nbreColRang; i++)
            {
                for (int j=0; j<nbreColRang; j++)
                {
                    Grid.SetRow(GdFond.Children[k], i);
                    Grid.SetColumn(GdFond.Children[k], j);
                    k++;
                }
            }
        }
        public void BontonClick ( Object sender, RoutedEventArgs e)
        {
            MessageBox.Show="Ok!";
            bool result;
            foreach (Carte elem in lesCartesJeu)
            {
                if (sender.Equals(elem.GetBouton()))
                {
                    result = Jouer.JouerUnTour(elem);
                    SetRaffraichir(result);
                }
            }
        }
        private void SetRaffraichir(bool leResultat)
        {
            this.raffraichir = leResultat;
            OnPropertyChanged("rafraichir");
        }
        protected void OnPropertyChanged(string propertyName)
        {
            MessageBox.Show("Ok!"+raffraichir);
            if(raffraichir) Jouer.VerifierUnTour();
        }
    }
}
