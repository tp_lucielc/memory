﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace memory
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Les propriétés statiques
        public Bouton leBouton00;
        public Bouton leBouton10;
        public Bouton leBouton01;
        public Bouton leBouton11;

        public Bouton[] les2Boutons;
        public int nbreClic = 0;
        public MainWindow()
        {
            InitializeComponent();

            //Initialisations des données statiques et liaison avec le control Button

            //Tableau des 2 Boutons à sélectionner
            les2Boutons = new Bouton[2];

            //Les 4 boutons
            leBouton00 = new Bouton();
            leBouton00.leBouton = btn001;
            leBouton00.laValeur = 1;

            leBouton10 = new Bouton();
            leBouton10.leBouton = btn101;
            leBouton10.laValeur = 1;

            leBouton01 = new Bouton();
            leBouton01.leBouton = btn011;
            leBouton01.laValeur = 2;

            leBouton11 = new Bouton();
            leBouton11.leBouton = btn111;
            leBouton11.laValeur = 2;
        }

        public void CliquerDeuxFois(Bouton leBouton)
        {
            nbreClic++;
            if (les2Boutons[0] == null)
            {
                les2Boutons[0] = leBouton;
                les2Boutons[0].leBouton.Visibility = Visibility.Visible;
            }
            else
            {
                if (les2Boutons[1] == null)
                {
                    les2Boutons[1] = leBouton;
                    les2Boutons[1].leBouton.Visibility = Visibility.Visible;

                    if(les2Boutons[0].laValeur == les2Boutons[1].laValeur)
                    {
                        MessageBox.Show("Yes! - Nbre de clic(s) : "+nbreClic);
                        les2Boutons[0].leStatus = true;
                        les2Boutons[1].leStatus = true;
                        if (Verification()) MessageBox.Show("Vous avez gagné en " + nbreClic);
                    }
                    else
                    {
                        MessageBox.Show("Pas de bol!- Nbre de clic(s) : " + nbreClic);
                        les2Boutons[1].leBouton.Visibility=Visibility.Hidden;
                        les2Boutons[0].leBouton.Visibility = Visibility.Hidden;

                    }
                    les2Boutons[1] = null;
                    les2Boutons[0] = null;
                }
            }
        }

        public bool Verification()
        {
            bool resultat = true;
            
                if (leBouton00.leStatus == false)
                {
                    resultat = false;
                }else if (leBouton10.leStatus == false)
                {
                    resultat = false;
                }
                else if (leBouton01.leStatus == false)
                {
                    resultat = false;
                }
                else if (leBouton11.leStatus == false)
                {
                    resultat = false;
                }

            
            return resultat;
        }
        private void btn000_Click(object sender, RoutedEventArgs e)
        {
            CliquerDeuxFois(leBouton00);
            
        }

        private void btn100_Click(object sender, RoutedEventArgs e)
        {
            CliquerDeuxFois(leBouton10);
        }

        private void btn010_Click(object sender, RoutedEventArgs e)
        {
            CliquerDeuxFois(leBouton01);
        }

        private void btn110_Click(object sender, RoutedEventArgs e)
        {
            CliquerDeuxFois(leBouton11);
        }
    }

    public class Bouton
    {
        public Button leBouton;
        public int laValeur;
        public bool leStatus = false;
    }
}
